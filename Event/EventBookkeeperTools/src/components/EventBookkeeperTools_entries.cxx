#include "EventBookkeeperTools/myCppFilterTest.h"
#include "../SkimDecisionMultiFilter.h"
//#include "../FileCutFlowSvc.h"
#include "../CutFlowSvc.h"
#include "EventBookkeeperTools/BookkeeperTool.h"
#include "../EventCounterAlg.h"

DECLARE_COMPONENT( myCppFilterTest )
DECLARE_COMPONENT( SkimDecisionMultiFilter )
DECLARE_COMPONENT( EventCounterAlg )
DECLARE_COMPONENT( BookkeeperTool )
DECLARE_COMPONENT( CutFlowSvc )
//DECLARE_COMPONENT( FileCutFlowSvc )

