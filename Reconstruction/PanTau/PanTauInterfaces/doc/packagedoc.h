/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page PanTauInterfaces_page PanTauInterfaces package
 * The interface package for PanTau.
 *
 * @author Sebastian.Fleischmann@cern.ch
 *
@section PanTauInterfaces_@section introductionPanTauInterfaces Introduction
 *
 * This package contains the interfaces for PanTau.
 *
@section PanTauInterfaces_@section PanTauInterfacesOverview Class Overview
 *   The PanTauInterfaces package contains the following classes:
 *
 *     - PanTau::ITauSeedTruthMatchTool : Match TauSeeds to truth taus.
 *
@section PanTauInterfaces_@section ExtrasPanTauInterfaces Extra Pages
 *
 **/
